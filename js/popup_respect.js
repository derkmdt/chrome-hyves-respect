	$(document).ready(function() {
		$(".close_icon").click(function() {
			window.close();
		});
		var bsound = localStorage["hyves_setting_sound"];
		chrome.tabs.getSelected(null, function(tab) {
		    var protocol =  tab.url.match(/^(.[^:]+):/)[1];
			if(protocol == "https" || protocol == "http") {	
				chrome.tabs.sendRequest(tab.id, {url: "getURL"}, function(response) {
					var takeurl = (response.url) ? response.url : tab.url; 
					var openwindow = function() {
						var params = {
							'message': 'De Respect Extensie heeft je toestemming nodig om te kunnen Respecteren op Hyves. Authoriseer hieronder en ga daarna terug naar de webpagina en respecteer nogmaals.'
						};
						var url = chrome.extension.getURL('options.html') + '?' + $.param(params);
						chrome.tabs.create({"url":url});
						window.close();
					}
					var doApiCalls = function() {
						hyves.call('opengraph.createRespect', {'target_url': takeurl}, function(response) {
							console.log(response);
							if(response._response.error_code == 1011) {
								$('#content_block').css('background-color', '#FF8B00');
								$('#content_block').width(150);
								$('#content_text').html('Url al gerespecteerd');
								if(bsound==1) chrome.tts.speak('Url already respected', {'gender': 'male'});
							} else if(response._response.error_code) {
								$('#content_block').css('background-color', '#FF0000');				
								$('#content_block').width(220);
								$('#content_text').html('Url kon niet worden gerespecteerd');
								if(bsound==1) chrome.tts.speak('Url could not be respected', {'gender': 'male'});
							} else {
								$('#content_block').css('background-color', '#336600');
								$('#content_block').width(80);
								$('#content_text').html('Respect!');
								if(bsound==1) chrome.tts.speak('Respect!', {'gender': 'male'});
							}
							$('#content_close').show();
						});
					}
					hyves =  new Hyves(hyves_api_key, hyves_api_secret);
					hyves.restoreToken(methods, 'users.getLoggedin', doApiCalls, openwindow);
				});
				} else {
					$('#content_block').css('background-color', '#FF0000');	
					$('#content_block').width(220);
					$('#content_text').html('Url kon niet worden gerespecteerd');
					if(bsound==1) chrome.tts.speak('Url could not be respected', {'gender': 'male'});
					$('#content_close').show();
				}
		});
    });

	var _gaq = _gaq || [];
	_gaq.push(['_setAccount', 'UA-25070052-1']);
	_gaq.push(['_trackPageview', '/extension/respectplus/popup']);

	(function() {
	var ga = document.createElement('script'); ga.type = 'text/javascript'; ga.async = true;
	ga.src = 'https://ssl.google-analytics.com/ga.js';
	var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(ga, s);
	})();