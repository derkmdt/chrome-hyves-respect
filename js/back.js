/*
Part of this page code is based on the code from the Facebook like button Chrome extension.
https://chrome.google.com/webstore/detail/jehaijobeonhempacbjelicepjkhoidi
*/
var resp = null;
chrome.browserAction.setBadgeBackgroundColor({color:[233, 131, 0, 255]});
chrome.tabs.onUpdated.addListener(function(tabId, change, tab) {
	if ( change.status == "loading" ) {
		getUrlData(tab);
	}
});
chrome.tabs.onActivated.addListener(function(tab_obj){
	chrome.tabs.get(tab_obj.tabId, function(tab) {
		getUrlData(tab);
	});
});
var getUrlData = function(tab) {
	if(!tab) return;
    var protocol =  tab.url.match(/^(.[^:]+):/)[1];
	if(protocol == "https" || protocol == "http") {
		chrome.browserAction.setIcon({tabId:tab.id, path:"img/icon-19.png"});
		chrome.tabs.sendRequest(tab.id,{url: "getURL",taburl: tab.url},function(response) {
			var takeurl = (response.url) ? response.url : tab.url; 
			var url = "https://data.hyves-api.nl/?oauth_consumer_key=OTc2M1-Ic_0de9HFOYYrI8M5AI8N&oauth_timestamp=1315321537&oauth_version=1.0&oauth_signature_method=PLAINTEXT&ha_version=2.1&ha_format=json&ha_method=opengraph.get&oauth_signature=OTc2M18fGs84vnIDAZ6ypHoIa3v-%26&url=" + encodeURIComponent(takeurl);
			var xhr = new XMLHttpRequest();
			xhr.open("GET", url, true);
			xhr.onreadystatechange = function() {
				if (xhr.readyState == 4) {
					resp = JSON.parse(xhr.responseText);
					if ( typeof resp.opengraph != "undefined" && resp.opengraph != "" && resp.opengraph[0].totalcount != 0) { 
						badgecounter(resp.opengraph[0].totalcount,tab);
					}
				}
			}
			xhr.send();
		});
	} else {
		chrome.browserAction.setIcon({tabId:tab.id, path:"img/icon-grey-19.png"});
	}
};
var badge = function(text,tab) {
     var d = new Object();
     d.text = text;
     d.tabId = tab.id;
     chrome.browserAction.setBadgeText(d);
};
var tip = function(text,tab) {
      var d = new Object();
      d.title = text;
      d.tabId = tab.id;
      chrome.browserAction.setTitle(d);
};
var badgecounter = function(text,tab) {
     var inttext = roundNumber(text);
	 var rUrl = "";
	 if ( resp != null ) {
		 if ( typeof resp.opengraph[0].url != "undefined" && resp.opengraph[0].url != "") {
			rUrl = resp.opengraph[0].url;
		 } else {
			rUrl = 'deze pagina';
		 } 
	 }
	 tip(formatNumber(text) + " Respects op "+rUrl, tab);
	 badge(inttext, tab);
};
var formatNumber = function(nStr) {
     nStr += '';
     x = nStr.split('.');
     x1 = x[0];
     x2 = x.length > 1 ? '.' + x[1] : '';
     var rgx = /(\d+)(\d{3})/;
     while (rgx.test(x1)) x1 = x1.replace(rgx, '$1' + ',' + '$2');
     return x1 + x2;
};
var roundNumber = function(val) {
     var _str = "";
     if (val >= 1e9) {
       _str = truncNb((val/1e9), 1)+'B';
     } else if (val >= 1e6) {
       _str = truncNb((val/1e6), 1)+'M';
     } else if (val >= 1e3) {
       _str = truncNb((val/1e3), 1)+'K';
     } else {
       _str = parseInt(val);
     }
     return ""+_str;
};