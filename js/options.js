var _gaq = _gaq || [];
_gaq.push(['_setAccount', 'UA-25070052-1']);
_gaq.push(['_trackPageview', '/extension/respectplus/options']);

(function() {
	var ga = document.createElement('script');
	ga.type = 'text/javascript'; ga.async = true;
	ga.src = 'https://ssl.google-analytics.com/ga.js';
	var s = document.getElementsByTagName('script')[0];
	s.parentNode.insertBefore(ga, s);
})();

var doApiCalls = function () {
	hyves.call('users.getLoggedin', {'ha_responsefields': 'profilepicture'}, function(responseUser) {
        var isSound = localStorage["hyves_setting_sound"];
        if(typeof isSound == 'undefined') isSound = localStorage["hyves_setting_sound"] = 0;
        (isSound == 1) ? $('#setsound').attr('checked', true) : $('#setsound').attr('checked', false);
		$('#photoHyver').append('<img src="'+responseUser.get('user')[0].profilepicture.square_large.src+'">');
        $('#linkHyver').append('<a href="'+responseUser.get('user')[0].url+'" target="_blank">'+responseUser.get('user')[0].firstname+' '+responseUser.get('user')[0].lastname+'</a>');
        $('#deauthorizeButton').text('Deautoriseren');
        $('#deauthorizeButton').click(deauthorize);
	});
};

var authorize = function () {
	hyves.requestAuthorizationRedirect(
		chrome.extension.getURL('options.html'), methods, 'infinite',
		function() {
			hyves.saveToken();
			window.location.href = chrome.extension.getURL('options.html');
		},
		function(e) {
			alert(e.message);
			window.location.href = chrome.extension.getURL('options.html');
		}
	);
	_gaq.push(['_trackEvent', 'Respect', 'Set', 'authorize']);
};

var deauthorize = function () {
	hyves.call('auth.revokeSelf', function(response) {
		$('#photoHyver').text('');
		$('#linkHyver').text('');
		$('#deauthorizeButton').text('Autoriseren');
		$('#deauthorizeButton').click(authorize);
		hyves.clearToken();
		localStorage.clear();
		_gaq.push(['_trackEvent', 'Respect', 'Set', 'deauthorize']);
	});	
}
$(document).ready(function() {
	$('#footercontent').append(share_content);
	$('#sidebarcontent').append(sidebar_content);
	$('#page').show();
	
	$.urlParam = function(name){
		var results = new RegExp('[\\?&]' + name + '=([^&#]*)').exec(window.location.href);	
		if( results !== null ) {
			return results[1] || 0;
		} else {
			return false;
		}
	}
	if($.urlParam('message')) {
		$('#notify_message').show();
		var message = $.urlParam('message').replace(/\+/g, ' ');
		$('#notify_message_text').text(message);
	};
	
	hyves =  new Hyves(hyves_api_key, hyves_api_secret);
	// check if redirected and finalize authorisation
	if (hyves.getQueryParamValue('oauth_token') != null) {
		authorize();
	}

	hyves.restoreToken(methods, 'users.getLoggedin',
		function() {
			doApiCalls();
		}, 
		function() {
			$('#currentHyver').text('none');
			$('#deauthorizeButton').text('Autoriseren');
			$('#deauthorizeButton').click(authorize);
		}
	);

	$('#setsound').change(function () {
        if($('#setsound:checked').val() == 'on') localStorage["hyves_setting_sound"] = 1; else localStorage["hyves_setting_sound"] = 0;
	});
});