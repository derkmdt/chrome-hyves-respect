chrome.extension.onRequest.addListener(function(request, sender, sendResponse){
	if(request.url == 'getURL') {
		if(b = document.querySelector('meta[property="og:url"]')) {
            sendResponse({url: b.content});
		} else if (b = document.querySelector('link[rel="canonical"]')){
			sendResponse({url: b.href});
        } else {
            sendResponse({});
        }
	}
});